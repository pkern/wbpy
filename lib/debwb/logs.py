# manage logs in the wanna-peruse database
## vim:set ai et sw=4 ts=4:
## pylint: disable-msg=R0201

# © 2009 Philipp Kern <pkern@debian.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301  USA.
"""
The logs module manages the logs in the wanna-peruse database.

wanna-peruse was a PHP/Perl script combination dating back to at least 2000.
It was created by Chris Rutter and maintained by Ryan Murray.

This new Python code tries to be extensible, robust and memory-efficient.

@contact: Debian Wanna-Build Admins <wb-team@buildd.debian.org>
@copyright: 2009 Philipp Kern <pkern@debian.org>
@license: GNU General Public License version 2 or later
"""

from __future__ import with_statement

from . import db
from bz2 import BZ2File
from io import BytesIO, StringIO
from datetime import datetime
from debian.deb822 import Deb822
from gzip import GzipFile
import base64
import copy
import email.parser
import errno
import os
import os.path
import quopri
import re
import string
import time
import urllib.parse
import zlib


class InvalidBuildLog(Exception):
    """
    This exception is raised together with the buildlog object and
    a rationale.  The exception is logged and causes processing of the
    buildlog to stop.  Keep in mind that not all properties of the
    buildlog object passed into the exception might be set.
    """

    def __init__(self, buildlog, rationale):
        self.buildlog = buildlog
        self.rationale = rationale

    def __str__(self):
        return self.rationale


class InvalidConfigurationOption(Exception):
    """
    This exception is raised whenever there is an unexpected configuration
    value found.  The message will give details on what exactly is wrong.
    """
    pass


class BuildLogFactory(object):
    """
    Some behaviour of the build log processor might need adjustments
    by the outside environment (like dry-runs, configuration and logging).
    Thus this factory collects those and makes them accessible to the
    individual BuildLog objects.
    """

    def __init__(self, logger, config, options, dbfactory=db.DBFactory):
        """
        @type  logger: logging.Logger
        @type  config: ConfigParser.ConfigParser
        @type options: optparse.Values
        
        post:
            len(self.compress) == 2 and callable(self.compress[0])
            type(self.dbdir) == str
            0 <= self.dbmode_dir <= 0777
            0 <= self.dbmode_file <= 0777
            len(self.valid_suites) > 0
            len(self.valid_arches) > 0
        """
        self.logger = logger
        self.config = config
        self.options = options

        self.dbfactory = dbfactory(logger, config)

        # The normal wanna-peruse database uses bzip2.  But we support gzip
        # here because we want to convert to that at some point.
        compress_type = self.config.get("buildlogs", "compress")
        if compress_type == "bzip2":
            self.compress = (BZ2File, '.bz2')
        elif compress_type == "gzip":
            self.compress = (GzipFile, '.gz')
        else:
            raise InvalidConfigurationOption(
                'Unknown compression option: {}'.format(compress_type))

        self.dbdir = self.config.get("buildlogs", "dbdir")
        # Use eval here because Python does not do octal string conversion
        # properly.
        if self.config.has_option("buildlogs", "dbmode"):
            self.dbmode_dir = eval(self.config.get("buildlogs", "dbmode"))
        else:  # pragma: no cover
            self.dbmode_dir = 0o775
        # Sanitise the file mode by removing the executable bit.  Maybe
        # someone wants to configure that at some point.
        self.dbmode_file = self.dbmode_dir & 0o666
        # Read the log location on the world wide web.
        self.logurl = self.config.get("buildlogs", "logurl")
        # We do not want to handle build logs for suites we do not allow
        # explicitly.
        self.valid_suites = self.config.get("global", "valid_suites").split()
        # Same goes for architectures.
        self.valid_arches = self.config.get("global", "valid_arches").split()

    def create_from_mail(self, fp):
        """
        This method processes a single buildlog by reading a mail
        message from the passed file object.  It does not return if the
        log in question was processed successfully or if it was discarded.
        If something goes horribly wrong, however, exceptions will be
        emitted.
        
        @type  fp: file
        @param fp: file object holding a mail message
        @return:   a BuildLog object or None
        
        pre:
            not fp.closed
        post:
            (type(__return__) == MailedBuildLog) or (__return__ == None)
        """

        try:
            return MailedBuildLog(self, fp)
        except InvalidBuildLog as exc:
            try:
                sender = exc.buildlog._sender
                subject = exc.buildlog._subject
            except AttributeError:
                sender = 'UNKNOWN'
                subject = 'UNKNOWN'
            self.logger.warn(
                "discarded F:'{sender}' S:'{subject}': {rationale}",
                sender=sender,
                subject=subject,
                rationale=exc.rationale)
            return None


class BuildLog(object):
    """
    This class represents a generic build log that was read from the
    disk.  It also serves as a base class for more sophisticated log handlers
    which can also serialise newly arrived ones to disk.  Objects should not be
    instantiated directly but instead be created by the BuildLogFactory
    as configuration needs to be fetched in order to ensure constraints
    to be satisfied.
    """

    def __init__(self, factory):
        self.__factory = factory

        # Set up storage attributes (actual buildlog properties)
        self._state = None
        self._arch = None
        self._package = None
        self._version = None
        self._suite = None
        self._timestamp = None

    ### PROPERTIES {{{ ########################################################

    @property
    def factory(self):
        """Returns the factory this build log is attached to."""
        return self.__factory

    @property
    def timestamp(self):
        """Returns the timestamp of the log."""
        return self._timestamp

    def _get_state(self):
        """
        post:
            __return__ in ['successful', 'failed', 'given-back', None]
        """
        return self._state

    def _set_state(self, state):
        """
        pre:
            state in ['successful', 'failed', 'given-back']
        """
        self._state = state

    def _del_state(self):
        self._state = None
    state = property(_get_state, _set_state, _del_state,
                     "Denotes if this build log is from a successful or a "\
                     "failed build.")

    def _get_arch(self):
        """
        post:
            __return__ in self.factory.valid_arches or __return__ is None
        """
        return self._arch

    def _set_arch(self, arch):
        """
        pre:
            arch in self.factory.valid_arches
        """
        self._arch = arch

    def _del_arch(self):
        self._arch = None

    arch = property(_get_arch, _set_arch, _del_arch,
                    "Architecture of the buildd that submitted the log.")

    def _get_package(self):
        return self._package

    def _set_package(self, package):
        self._package = package

    def _del_package(self):
        self._package = None

    package = property(_get_package, _set_package, _del_package,
                       "Package name the build log is associated to.")

    def _get_version(self):
        return self._version

    def _set_version(self, version):
        self._version = version

    def _del_version(self):
        self._version = None

    version = property(
        _get_version, _set_version, _del_version,
        "Version of the package the build log is associated to.")

    def _get_suite(self):
        """
        post:
            __return__ in self.factory.valid_suites or __return__ is None
        """
        return self._suite

    def _set_suite(self, suite):
        """
        pre:
            suite in self.factory.valid_suites
        """
        self._suite = suite

    def _del_suite(self):
        self._suite = None

    suite = property(_get_suite, _set_suite, _del_suite,
                     "Suite the upload was tried in.")

    ### }}} ###################################################################


class MailedBuildLog(BuildLog):
    """
    This class is used by the BuildLogFactory to handle an incoming mail
    and dump the build log it contains to disk.  It should not be invoked
    directly.
    """

    def __init__(self, factory, fp):
        """
        The file object passed as I{fp} must be seeked to the start (i.e.
        if it is stdin, nothing must have been read from it before).
        """
        super(MailedBuildLog, self).__init__(factory)
        self._fp = fp

        # Preset the timestamp (int granularity should be enough)
        self._timestamp = int(time.time())

        # Additional attributes
        self._message = None
        self._sender = None
        self._subject = None
        self._first_line = None
        self._action = None  #: just used by _ensure_directory

        # Start the parse on the passed file object.  The parse method will
        # raise exceptions if the given mail does not represent a valid build
        # log.
        self._parse()

    ### PUBLIC METHODS {{{ ####################################################

    def store(self):
        """
        Commits a log into the wanna-peruse database, updating all the metadata
        and indexes on the way.

        post:
            isinstance(__return__, str)
        """
        filename = self._write_logfile()
        self._write_dbinfo()
        self._write_manifest()
        self._write_global_log()
        self.factory.logger.info(
            "committed F:'{log._sender}' "
            "P:'{log.suite}/{log.package}_{log.version}': {log.state}",
            log=self)
        return filename

    ### }}} ###################################################################

    ### PROTECTED METHODS {{{ #################################################

    def _set_state(self, state):
        # Normalise the attempted state to failed.
        if state == 'attempted':
            state = 'failed'
        super(MailedBuildLog, self)._set_state(state)

    state = property(BuildLog._get_state, _set_state, BuildLog._del_state,
                     BuildLog.state.__doc__)

    def _parse(self):
        self._parse_header()
        self._parse_sender()
        self._parse_subject()

        if self.arch is None:
            self._parse_arch()

    def _parse_header(self):
        """
        Fetch the subject and the sender from the mail's header by piping
        it through a email.parser.FeedParser instance and stopping the
        file processing after the end of the header is reached to avoid
        parsing the entire mail.
        
        pre:
            self._fp is not None
        post:
            self._message is not None
            self._subject is not None
            self._sender is not None
        """
        # Parse the header (i.e. the blob until the first empty line).
        parser = email.parser.FeedParser()
        while True:
            line = self._fp.readline()
            if line.strip() == "":
                break
            parser.feed(line)
        self._message = parser.close()

        self._subject = self._message.get("Subject")
        self._sender = self._message.get("From")

        if self._subject is None:
            self._discard('no subject found')
        if self._sender is None:
            self._discard('no sender found')

    def _parse_sender(self):
        """
        Generate a builder name from the From field of the header.  It
        assumes that the machine name is sufficiently unique.  This
        avoids leaking the complete email address.

        pre:
            self._sender is not None
        post:
            self.builder is not None
        """
        # Let's assume that there are two possibilities for email
        # address encodings: "buildd@host.org" and "buildd on host
        # <buildd@host.org>".  For other insane things this regular
        # expression should also return something sane.
        m = re.match('(.*<)?(\S*)@(\S+\.[^\s>]*)(>.*)?', self._sender)
        if m:
            # group 3 is the hostname part of the mail address
            self.builder = m.group(3)
        else:
            # It did not match: just use the sender verbatim and log
            # the strange From line.
            self.factory.logger.info(
                "invalid sender F:'{log._sender}' S:'{log._subject}'".format(
                    log=self))
            self.builder = self._sender

    def _parse_subject(self):
        """
        Retrieve as much useful information out of the subject as possible.
        
        Caveat: There are two possible subject formats out there, with different
        amount of information.
        
        Solution: Parse both and grap the missing stuff from somewhere else
        later.
        
        post:
            self.state is not None
            self.package is not None
            self.version is not None
            self.suite is not None
        """

        m = re.match(r'Log for ([\w-]+) build of (\S+)\s+\(dist=([\w-]+)\)',
                     self._subject)
        if m:
            self.state = self._check_state(m.group(1))
            (self.package, self.version) = self._parse_pkgver(m.group(2))
            self.suite = self._check_suite(m.group(3))
            return

        m = re.match(r'Log for ([\w-]+) build of (\S+) on ([\w-]+)\s+'\
                     r'\(dist=([\w-]+)\)', self._subject)
        if m:
            self.state = self._check_state(m.group(1))
            (self.package, self.version) = self._parse_pkgver(m.group(2))
            self.arch = self._check_arch(m.group(3))
            self.suite = self._check_suite(m.group(4))
            return

        m = re.match(r'Log for ([\w-]+) build of (.+) on ([\w-]+)\s+'\
                     r'\(([^\/]+)\/([^)]+)\)', self._subject)
        if m:
            self.state = self._check_state(m.group(1))
            (self.package, self.version) = self._parse_pkgver(m.group(2))
            self.arch = self._check_arch(m.group(3))
            self.suite = self._check_suite(m.group(5))
            return

        self._discard('invalid subject')

    def _parse_pkgver(self, pkgver):
        ary = pkgver.split('_')
        if len(ary) < 2:
            self._discard('invalid pkgver: {}'.format(pkgver))
        return (ary[0], ary[1])

    def _parse_arch(self):
        """
        Not all sbuild versions pass the architecture information in the
        subject.  Thus this method attempts to retrieve it from the first
        introductional line of the build log.  This line is saved for
        later reference when dumping the log into the storage area.
        This method does not support base64 encoding for the whole
        message body.
        
        pre:
            self._fp is not None
            not self._fp.closed
            self.arch is None
        post:
            self._first_line is not None
            self.arch is not None
        """
        self._first_line = self._fp.readline()
        m = re.match('^Automatic build of \S+ on \S+ by sbuild/([-\w]+)',
                     self._first_line)
        if m:
            self.arch = self._check_arch(m.group(1))
            return
        self._discard('first line does not include arch info ({})'.format(
            self._first_line))

    def _write_global_log(self):
        """
        Appends a log entry to the global log file.
        
        pre:
            self.factory.dbdir is not None
        """
        with open(os.path.join(self.factory.dbdir, "log"), 'a') as f:
            f.write(self._global_log_entry())

    def _write_logfile(self):
        """
        Write the log file to the disk.  This function also handles
        the on-disk compression which can be configured.
        
        pre:
            # sanity check that the parsing was done first
            self._fp is not None
            not self._fp.closed
            self._message is not None
            # check for compressor factory and file extension
            len(self.factory.compress) == 2
            # could we actually create such a compressor object
            callable(self.factory.compress[0])
            # properties we need to read
            self.arch is not None
            self.timestamp is not None
        post:
            isinstance(__return__, str)
        """
        file_factory, file_extension = self.factory.compress
        fn = "{}_{}_log{}".format(self.arch, self.timestamp, file_extension)
        path = self._ensure_directory()
        full_fn = os.path.join(path, fn)
        # XXX: This is wrong.  Maybe we could increase the timestamp instead.
        assert not os.path.exists(full_fn)

        with file_factory(full_fn, mode='wb') as fp:
            # Adjust the mode of the file according to the configuration.
            os.fchmod(fp.fileno(), self.factory.dbmode_file)

            self.build_time, self.disk_space = None, None

            # Check if it's MIME encoded.
            content_type = self._message.get("Content-Type")
            if self._content_type_is_mime(content_type):
                self._dump_mime_mail_to_file(fp)
            else:
                # Write the header.
                fp.write(self._message.as_string(unixfrom=True).encode('UTF-8'))

                # Lookup the mail encoding.
                encoding = self._message.get("Content-Transfer-Encoding")
                if (encoding is None or
                    encoding.lower() in ['8bit', 'binary']):
                    self._dump_mail_to_file(fp)
                else:
                    self._dump_encoded_mail_to_file(fp, encoding.lower())

        return full_fn

    def _content_type_is_mime(self, content_type):
        """
        This is quite hackish, we only check for the value we expect here.
        email.Message.is_multipart() wouldn't work here because we did not
        parse the whole message yet, but just the header.

        post:
            __return__ == (content_type is not None and \
                (content_type[0:10] == 'multipart/'))
        """
        if content_type is None:
            return False
        if content_type.split(';')[0] == 'multipart/mixed':
            return True
        return False

    def _dump_mime_mail_to_file(self, f):
        """
        pre:
            not self._message.is_multipart()
            self._first_line is None
        """
        parser = email.parser.FeedParser()
        # re-read the current message
        parser.feed(self._message.as_string(unixfrom=True))
        # Read the remaining buffer.  It's will only read the Gzip-compressed
        # parts into memory, so it's not as resource intensive than to
        # de-serialize a whole plaintext build log.
        # XXX: check that it's really gziped
        for line in self._fp:
            parser.feed(line)
        msg = parser.close()
        parts = msg.get_payload()
        if len(parts) < 2:
            raise InvalidBuildLog(
                self, 'less than two MIME parts found: {} parts'.format(
                    len(parts)))
        old_msg = copy.deepcopy(self._message)
        del old_msg['Content-Type']
        f.write(old_msg.as_string(unixfrom=True).encode('UTF-8'))
        for part in parts:
            filename = part.get_filename('')
            if filename.endswith('.gz'):
                try:
                    # This ought to be the log.
                    log_fp = GzipFile(
                        fileobj=BytesIO(part.get_payload(decode=True)),
                        mode='r')
                    for line in log_fp:
                        f.write(line)
                    log_fp.close()
                except IOError:  # IOError: Not a gzipped file
                    raise InvalidBuildLog(
                        self, 'not gzipped according to GzipFile: {}'.format(
                            filename))
                except zlib.error:  # Errors within the zlib compression
                    raise InvalidBuildLog(
                        self, 'invalid zlib encoding: {}'.format(filename))
            elif filename.endswith('.summary'):
                buf = BytesIO(part.get_payload(decode=True))
                try:
                    items = Deb822(buf)
                    if 'Build-Space' in items:  # in kilobytes
                        self.disk_space = int(items['Build-Space'].strip()
                                              .replace('n/a', '0')) * 1024
                    if 'Package-Time' in items:
                        self.build_time = int(items['Package-Time'].strip())
                except ValueError:
                    raise InvalidBuildLog(
                        self,
                        'invalid value in summary dict: {}'.format(filename))

    def _dump_mail_to_file(self, f):
        # Actually write the log, special-case the first line which might
        # have been read already.
        if self._first_line is not None:
            f.write(self._first_line)
        last_line_re = re.compile(
            r'Build needed (\d+):(\d\d):(\d\d), (\d+)k dis[ck] space')
        # The iteration through generators should be efficient.
        for line in self._fp:
            m = re.match(last_line_re, line)
            if m:
                self.build_time = int(m.group(1)) * 3600 + \
                    int(m.group(2)) * 60 + \
                    int(m.group(3))
                self.disk_space = int(m.group(4)) * 1024
            f.write(line.encode('UTF-8'))

    def _dump_encoded_mail_to_file(self, f, encoding):
        """
        Copies the log content from self._fp, decodes it and writes it into f.
        The log's first line is special-cased like in _dump_mail_to_file.
        The encoding is expected to be lower-cased already.
        
        pre:
            encoding.islower()
        """
        if encoding == 'base64':
            module = base64
        elif encoding == 'quoted-printable':
            module = quopri
        else:
            raise InvalidBuildLog(self,
                                  'unknown encoding: {}'.format(encoding))
        if self._first_line is not None:
            f.write(module.decodestring(self._first_line))
        module.decode(self._fp, f)

    def _ensure_directory(self):
        """
        Constructs the name of the directory which will hold the build log
        that is currently being processed and creates it if necessary.
        
        @return: directory name
        
        post:
            os.path.exists(os.path.join(self.factory.dbdir,
                                        self.package[0],
                                        self.package,
                                        self.version))
        """
        dirname = os.path.join(self.factory.dbdir, self.package[0],
                               self.package, self.version)
        # Check if we were already called and took action, to not
        # override the _action field.
        if self._action != None:
            return dirname
        # The following distinction is needed as the global log wants
        # to know if a (package, version) tuple is new or if there were
        # build logs other than the one that just arrived.  As this
        # distinction is purely cosmetical it might just be dropped
        # at some point in the future.
        if os.path.exists(dirname):
            self._action = "updpkg"
        else:
            self._action = "addpkg"
            try:
                os.makedirs(dirname, self.factory.dbmode_dir)
            except OSError as e:
                if e.errno == errno.EEXIST:
                    self._action = "updpkg"
                else:
                    raise
        return dirname

    def _write_dbinfo(self):
        """
        This function gets itself a table object from the schema,
        dependent on the architecture.  It needs to be called after
        _write_logfile to know build_time and disk_space.

        pre:
            self.factory.dbfactory is not None
            self.package is not None
            self.suite is not None
            self.version is not None
            self.timestamp is not None
            self.state is not None
            self.builder is not None
            'build_time' in self.__dict__
            'disk_space' in self.__dict__
        """
        if self.build_time is None or self.disk_space is None:
            self.factory.logger.warn("No build time / disk space found.  "\
                "F:'{log._sender}' P:'{log.suite}/{log.package}_{log.version}'".format(
                    log=self))

        self.factory.dbfactory.insert_pkg_history(
            arch=self.arch,
            package=self.package,
            distribution=self.suite,
            version=self.version,
            timestamp=datetime.utcfromtimestamp(self.timestamp),
            result=self.state,
            builder=self.builder,
            build_time=self.build_time,
            disk_space=self.disk_space)

    def _manifest_entry(self):
        """
        A manifest entry contains parsed metadata about a build log.
        Currently the architecture, a timestamp unique for the log,
        the target distribution and the build state (i.e. if the build
        succeeded or not) are kept there to generate log listings.
        
        pre:
            self.arch is not None
            self.timestamp is not None
            self.suite is not None
            self.state is not None
        post:
            type(__return__) in (unicode, str)
        """
        return "{log.arch}~{log.timestamp}~{log.suite}~{log.state}\n".format(
            log=self)

    def _write_manifest(self):
        """
        The log files in a directory are indexed with their metadata
        (see BuildLog._manifest_entry) in a file called "info".  This
        function inserts the record for the current build log and
        should be called after the log file has been written to disk.
        """
        path = self._ensure_directory()
        with open(os.path.join(path, "info"), 'a') as manifest:
            manifest.write(self._manifest_entry())

    def _global_log_entry(self):
        """
        To provide an overview what happened recently a global log is kept
        while is never purged but only appended.  The web page displays
        a tail of it.  This function compiles such an entry which is
        later appended by BuildLog._write_global_log.
        
        pre:
            # _ensure_directory needs to be called before
            self._action is not None
            # sanity check the properties
            self.timestamp is not None
            self.arch is not None
            self.package is not None
            self.version is not None
        post:
            type(__return__) in (unicode, str)
        """
        return "{log.timestamp} {log._action} {log.arch} {log.package} {log.version}\n".format(
            log=self)

    def _discard(self, reason):
        """
        This method stops processing of the current buildlog while logging
        a message.  As we cannot just return at this point an exception
        is raised to return to the point where the processing of the next
        buildlog could be started.
        """

        raise InvalidBuildLog(self, reason)

    def _check_arch(self, arch):
        """
        post:
            __return__ == arch
        """
        if arch not in self.factory.valid_arches:
            raise InvalidBuildLog(self, "invalid arch: {}".format(arch))
        return arch

    def _check_state(self, state):
        """
        post:
            __return__ == state
        """
        if state not in ['successful', 'failed', 'attempted', 'given-back']:
            raise InvalidBuildLog(self, "invalid state: {}".format(state))
        return state

    def _check_suite(self, suite):
        suite = self.factory.dbfactory.resolve_distribution_alias(suite)
        if suite not in self.factory.valid_suites:
            raise InvalidBuildLog(self, "suite not accepted: {}".format(suite))
        return suite

    @property
    def substvars(self):
        """
        pre:
            # sanity check the properties
            self.timestamp is not None
            self.arch is not None
            self.package is not None
            self.version is not None
            self.state is not None
            self.suite is not None
            self.builder is not None
        post:
            'timestamp' in __return__
            'arch' in __return__
            'package' in __return__
            'version' in __return__
            'state' in __return__
            'suite' in __return__
            'builder' in __return__
        """
        substvars = {
            'timestamp': self.timestamp,
            'arch': self.arch,
            'package': self.package,
            'version': self.version,
            'state': self.state,
            'suite': self.suite,
            'builder': self.builder,
        }
        substvars_quoted = dict(
            [(k, urllib.parse.quote(str(substvars[k]))) for k in substvars])
        substvars['buildlog'] = string.Template(
            self.factory.logurl).substitute(substvars_quoted)
        return substvars

    def summary(self):
        """
        post:
            # return a tuple
            len(__return__) == 2
        """
        subject_template = string.Template(
            "$state $arch build of $package $version")
        body_template = string.Template(" * Source package: $package\n"
                                        " * Version: $version\n"
                                        " * Architecture: $arch\n"
                                        " * State: $state\n"
                                        " * Suite: $suite\n"
                                        " * Builder: $builder\n"
                                        " * Build log: $buildlog\n")
        return (subject_template.substitute(self.substvars),
                body_template.substitute(self.substvars))

    ### }}} ###################################################################


#import contract
#contract.checkmod(__name__)
